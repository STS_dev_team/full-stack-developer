# Fullstack Developer Challenge

## How To Participate
1. Fork this repo
2. Do your best
3. Prepare pull request and let us know that you are done

## Requirements
* Use NodeJS, .Net Core or language of your choice for backend. 
* Use JS framework (Vue, React, Angular).
* Use CSS preprocessor (SCSS, SASS, LESS).
* Use MySQL or other SQL Database for data store.
* Use task-runner, to compile your css/js (Gulp, Webpack or Grunt), but do not commit the build.

## Some things we love
* API-first approach
* Structure
* Comments
* Meaningful methods
* Fun / Magic / Creativity!


